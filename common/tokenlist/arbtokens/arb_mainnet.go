//go:build !testnet && !mocknet
// +build !testnet,!mocknet

package arbtokens

import (
	_ "embed"
)

//go:embed arb_mainnet_latest.json
var ARBTokenListRawV109 []byte
