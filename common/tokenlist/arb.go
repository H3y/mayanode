package tokenlist

import (
	"encoding/json"

	"github.com/blang/semver"
	"gitlab.com/mayachain/mayanode/common/tokenlist/arbtokens"
)

var arbTokenListV109 EVMTokenList

func init() {
	if err := json.Unmarshal(arbtokens.ARBTokenListRawV109, &arbTokenListV109); err != nil {
		panic(err)
	}
}

func GetARBTokenList(version semver.Version) EVMTokenList {
	return arbTokenListV109
}
